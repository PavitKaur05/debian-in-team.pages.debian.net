# Debian India Community's homepage.
This is a simple website built using the static site generator
[Hugo](https://gohugo.io/) using the
[Gruvhugo](https://gitlab.com/avron/gruvhugo) theme.

The website is developed on [Debian
Salsa](https://salsa.debian.org/debian-in-team/debian-in-team.pages.debian.net)
and a mirror is maintained for deployment on
[Gitlab](https://gitlab.com/debian-in/debian-in.gitlab.io).

**If you would like to open a issue or merge request please do so on salsa.**
### Setting up locally
- Install Hugo
  ```sh
  sudo apt instal hugo
  ```
- Clone the repo along with the theme which is embedded as a submodule in the repo.

    ```sh
    git clone --recurse-submodules https://salsa.debian.org/debian-in-team/debian-in-team.pages.debian.net.git
    ```
- Start the development server

    ```sh
    hugo sever -D
    ```
You should be able to view the site on `localhost:1313/` unless the port `1313` is already occupied.

### Project overview
- All posts go in the `content` folder.
  - Blog posts go in `content/post`
- All configuration is handled via `config.toml`.
- Place post templates in `archetypes`. Currently we have a default template
  that we can use for simple posts but as we diversify the type of content we
  host on the site it would be a great idea to add templates.

### General Guidelines
- Create a post using hugo cli instead of adding files manually.
  ```sh
  hugo new <dir-name>/<post-name>.md
  ```
  For example if you run `hugo new post/sept-update.md`, hugo will create a file
  named `sept-update.md` in `content/post`.
- Stick to 80 column limit.
  ([Why?](https://nickjanetakis.com/blog/80-characters-per-line-is-a-standard-worth-sticking-to-even-today))
  - Most modern text editors have ways to break line when it exceeds a certain
    limit, so lookup how to set it up in your text editor.
- Test locally before pushing changes.
- If you would like to write on a specific topic on the Debian India blog, open
  an issue or send a mail to the Debian India mailing list before starting.
- Post submission can be made through mail or a merge request.
