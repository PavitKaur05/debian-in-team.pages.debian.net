---
title: "About"
date: 2021-08-25T21:14:40+05:30
draft: false
---

 Debian community in India has a long and diverse history. Earliest events date
 back to 2006 when various free software developers from India joined Debian
 under various roles. Due to this, Debian is partially/fully available in 11
 local Indian languages. A number of events including MiniDebConf, release
 parties, Debian Utsav/Utsavam has helped grow the community. We recently hosted
 the online MiniDebConf India which had 48+ talks/events from 6 local languages
 under two tracks. We’re also getting ready to host the Debian community in
 DebConf 2023 scheduled to held in India. These events have given us the
 platform to publicize the use, adoption and contribution of Debian in India.
 
